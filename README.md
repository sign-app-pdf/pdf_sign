DigitalSignature


The intent of the challenge is to develop a feature that enables an user to sign a pdf document digitally.
The initial screen should allow the user to upload a document via drag
and drop option or an Upload button.
The application should accept only one pdf document and its size
should not exceed 12MB.

Once the user uploads the pdf, the respective document should be
displayed on the screen.
There should be a button named “Add signature” when clicked should
open a modal box.

The Add signature modal box should contain a rectangular region in
which a user can draw his/her signature.
There should be an option to clear the signature drawn.
Once the user draws the signature, they can click on the Add button.
The modal box should have a close button when clicked should close
the modal window and return back to the previous screen .

Once the user clicks on the Add button, they should be able to place
the signature drawn, in the respective space in the document.
The signature should be resizable, deletable and movable in anywhere
in the document preview.
When the user places the signature on the document, then display a
button named Save.
Else if the user deletes the signature created, then go to the previous
screen.

When the user clicks on the save button, the signature should be
added in the document and the Save button changes to Download
button.
Clicking on the Download button should enable the user to download
the signed pdf document.
